package br.com.rotlite.rotlite;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by biliboss on 24/04/2016.
 */
public class RotLiteUtils {
    public static OkHttpClient newOkHttpClient() {
        HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
        logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(logInterceptor);
        if (RotLite.hasInterceptor()) {
            builder.addInterceptor(RotLite.getInterceptor());
        }

        return builder.build();
    }

    private static RotLiteUtils mInstance;

    public static RotLiteUtils getInstance() {
        if (mInstance == null){
            mInstance = new RotLiteUtils();
        }
        return mInstance;
    }

    public static void setInstance(RotLiteUtils instance){
        mInstance = instance;
    }

    public String getDbName(Context context){
        return getMeta(context, "rotlite_dbname");
    }

    public String getServer(Context context){
        return getMeta(context, "rotlite_server");
    }

    public String getAuthKey(Context context){
        return getMeta(context, "rotlite_auth_key");
    }

    private static String getMeta(Context context, String name) {
        try {
            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            return bundle.get(name).toString();

        } catch (PackageManager.NameNotFoundException e) {
            //Log.e(TAG, "Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e) {
            //Log.e(TAG, "Failed to load meta-data, NullPointer: " + e.getMessage());
        }
        return null;
    }
}
