package br.com.rotlite.rotlite;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import okhttp3.Response;

/**
 * Created by claudio on 29/09/15.
 */
public class RotLiteSyncIntentService<T extends RotLiteObject> extends IntentService {

    public static String TAG = RotLiteSyncIntentService.class.getSimpleName();
    private Map<String, Class<? extends RotLiteObject>> classes = new ConcurrentHashMap();

    public RotLiteSyncIntentService() {
        super("RotLiteSyncIntentService");
    }

    public static boolean isRunning = false;

    List<RotLiteModel> objsToUpload;

    private List modelsToDownload = new ArrayList();
    private boolean isDownloading = false;
    private boolean isUploading = false;
    private int totalToDownload = 0;

    @Override
    protected void onHandleIntent(Intent intent) {
//        Log.i("Tasky", "Iniciando Sincronização - RotLib - " + isRunning);
        isRunning = true;
        try {
//            Log.i("Tasky", "Downloading Data");
            download(new Sync() {
                @Override
                public void onFinish() {
//                    Log.i("Tasky", "Uploading Data");
                    upload();
                }
            });
        } finally {
//            Log.i("Tasky", "Finalizando Sincronização - RotLib");
//            isRunning = false;
        }
    }

    private void upload() {

        isUploading = true;
        classes = RotLite.getInstance().classes;

        final RotLiteModel toUpload = new RotLiteModel(getApplicationContext());
        toUpload.findInBackground(new RotLiteCallback<RotLiteModel>() {
            @Override
            public void done(List<RotLiteModel> list) {
                objsToUpload = list;
                uploadObjs(list);
            }

            @Override
            public void error(RotLiteException e) {

            }
        });

    }

    private void uploadObjs(List<RotLiteModel> list) {
//        Log.v(TAG, "To upload: " + list.size());

        for (int i = list.size() - 1; i >= 0; i--) {
            if (i < list.size()) {
                final RotLiteModel m = list.get(i);

                final int pos = i;

                final String modelName = m.getString("model");
                final String objectId = m.getString("data_id");

//                //Log.v(TAG, "saveweb " + objectId);

                for (Map.Entry<String, Class<? extends RotLiteObject>> entry : classes.entrySet()) {

                    if (entry.getKey().equals(modelName)) {

                        final Class<? extends RotLiteObject> clzz = entry.getValue();
                        final T model;
                        try {
                            model = (T) clzz.getDeclaredConstructor(Context.class).newInstance(getApplication());

                            model.getById(objectId);

                            try {
                                final List<T> find = model.find();
                                if (find.size() > 0) {
                                    final T obj = (T) find.get(0);
                                    obj.setIsSync(true);
                                    if (obj.isReadyToUpload()) {
//                                        Log.v(obj.TAG, "go");
                                        obj.saveWeb(new RotLiteSyncUploadCallback() {
                                            @Override
                                            public void onFailure(RotLiteException e) {
                                                e.printStackTrace();
//                                                Log.e(TAG, "Failure to upload '" + obj.getId() + "': " + e.getMessage() + " - " + e.hashCode());
                                                if (objsToUpload.size() > pos) {
                                                    objsToUpload.remove(pos);
                                                    find.remove(0);
                                                }
                                            }

                                            @Override
                                            public void onSuccess(Response response, RotLiteObject objUpload) {
                                                RotLiteModel temp = new RotLiteModel(getApplicationContext());
                                                temp.where("model = '" + modelName + "' and data_id = '" + objectId + "'");
                                                temp.delete();

                                                obj.setIsSync(true);
                                                if (obj.update()) {
//                                                    Log.v("setSync", "ops1 ");
                                                } else {
//                                                    Log.v("setSync", "opa..");
                                                }
                                                if (objsToUpload.size() > pos) {
                                                    objsToUpload.remove(pos);
                                                    //find.remove(0);
                                                }

                                                if (objsToUpload.size() <= 0) {
                                                    objsToUpload.clear();
                                                    Intent in = new Intent(RotLite.BROADCAST_SYNC_ACTION);
                                                    in.putExtra("model", model.getClass().getName());
                                                    in.putExtra("action", "upload");
                                                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(in);
                                                }

//                                                Log.v(TAG, "Success to upload '" + objUpload.getId() + "': " + objUpload.jsonString());
                                            }
                                        });
                                    } else {
//                                        Log.e(TAG, "O model impediu o upload dos dados através do método isReadyToUpload(): " + obj.getClass().getName());
                                    }
                                }
                            } catch (RotLiteException e) {
                                e.printStackTrace();
//                                Log.e(TAG, "Error: " + e.getMessage());
                            }


                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        }

                    }

                }
            }

        }
    }

    private void download(final Sync callback) {
        classes = RotLite.getInstance().classes;
        totalToDownload = classes.size();
//        //Log.v(TAG, "totalToDownload (1) = " + totalToDownload);

        if (modelsToDownload.size() == 0 && !isDownloading) {

            for (Map.Entry<String, Class<? extends RotLiteObject>> entry : classes.entrySet()) {
                if (!modelsToDownload.contains(entry.getKey())) {
                    modelsToDownload.add(entry.getKey());
                }
            }

            isDownloading = true;

        }

        if (modelsToDownload.size() == 0 && !isUploading) {
            callback.onFinish();
        }

        for (final Map.Entry<String, Class<? extends RotLiteObject>> entry : classes.entrySet()) {

            if (modelsToDownload.size() > 0 && modelsToDownload.get(0).equals(entry.getKey())) {

                final Class<? extends RotLiteObject> clzz = entry.getValue();
                final T model;
                try {

                    //Trazendo dados da web

//                    Log.v(TAG, "Downloading: " + entry.getKey());

                    model = (T) clzz.getDeclaredConstructor(Context.class).newInstance(getApplication());
                    final boolean autosync = model.getAutoSync();

                    model.prepareSync(); //Se tivermos que executar algo antes da sync, isso acontecerá aqui!
                    model.fromWeb();

                    if (model.isReadyToDownload()) {

                        try {
                            if (!model.tbExists(model.getTbName())) {
                                model.createTable(model.getTbName());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
//                            Log.e(TAG, "Error creating new table: " + e.getMessage());
                        }

                        final java.text.DateFormat dateFormat = new SimpleDateFormat(model.getDateFormat());
                        final Date date = new Date();

                        model.downloadAndSyncData(new RotLiteDownloadCallback() {
                            @Override
                            public void onSuccess() {

                                model.setLastSyncDate(dateFormat.format(date));
                                model.onSync(null);

                                totalToDownload--;

                                if (totalToDownload <= 0)
                                    callback.onFinish();

                                if (modelsToDownload.contains(entry.getKey())) {
                                    modelsToDownload.remove(entry.getKey());
                                    download(callback);
                                }

                                Intent in = new Intent(RotLite.BROADCAST_SYNC_ACTION);
                                in.putExtra("model", model.getClass().getName());
                                in.putExtra("action", "download");
                                in.putExtra("status", RotLite.BROADCAST_SYNC_STATUS_SUCCESS);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(in);

//                                Log.v(TAG, "LocalBroadcastManager: " + model.getClass().getName() + "; action: download; status: " + RotLite.BROADCAST_SYNC_STATUS_SUCCESS);

                            }

                            @Override
                            public void onFailure(RotLiteException e) {
                                e.printStackTrace();
                                totalToDownload--;
//                                //Log.e(TAG, "totalToDownload (5) = " + totalToDownload + "; model: " + model.getTbName());
                                if (totalToDownload <= 0)
                                    callback.onFinish();
//                                Log.e(TAG, e.getMessage() + " - Code: " + e.hashCode());

                                if (modelsToDownload.contains(entry.getKey())) {
                                    modelsToDownload.remove(entry.getKey());
                                    download(callback);
                                }

                                Intent in = new Intent(RotLite.BROADCAST_SYNC_ACTION);
                                in.putExtra("model", model.getClass().getName());
                                in.putExtra("action", "download");
                                in.putExtra("status", RotLite.BROADCAST_SYNC_STATUS_ERROR);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(in);
                            }
                        });

                    }else{
//                        Log.e(TAG, "O model impediu o download da sincronização através do método isReadyToDownload(): " + model.getClass().getName());
                    }

                } catch (InstantiationException e) {
                    e.printStackTrace();
//                    Log.e(TAG, "Error out model (1) " + e.getMessage());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
//                    Log.e(TAG, "Error out model (2) " + e.getMessage());
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
//                    Log.e(TAG, "Error out model (3) " + e.getMessage());
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
//                    Log.e(TAG, "Error out model (4) " + e.getMessage());
                }
            }

        }
    }

    public interface Sync {
        void onFinish();
    }
}
