package br.com.rotlite.rotlite;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by claudio on 28/10/15.
 */
public interface RotLiteQueryInterface<T extends RotLiteObject> {
    void fromLocal();
    void fromWeb();
    void setEndpoint(String name);
    void setEndpointPost(String name);
    void setEndpointPut(String name);
    void setEndpointDelete(String name);
    String getEndpoint();
    void limit(int max);
    void limit(int min, int max);
    void order(String order);
    void group(String column);
    void where(String where);
    void getById(String uuid);
    String getExecutedQuery();
    void include(String column);
    List<T> find() throws RotLiteException;
    T getFirst() throws RotLiteException;
    void put(String key, String value);
    void put(String key, double value);
    void put(String key, int value);
    void put(String key, long value);
    void put(String key, boolean value);
    void put(String key, JSONObject value);
    void put(String key, JSONArray value);
    void setPOSTRequestMode();
    void setGETRequestMode();
    void findInBackground(RotLiteCallback callback);
}
