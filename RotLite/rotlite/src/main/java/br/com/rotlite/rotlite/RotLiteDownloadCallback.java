package br.com.rotlite.rotlite;

/**
 * Created by claudio on 29/10/15.
 */
public interface RotLiteDownloadCallback {
    void onSuccess();
    void onFailure(RotLiteException e);
}
